From 929c92b7928b3d8a178614ccb311a6d848d6e0cd Mon Sep 17 00:00:00 2001
From: Alex Harries <alex.harries@redactive.co.uk>
Date: Tue, 18 Feb 2020 14:32:44 +0000
Subject: [PATCH] Drupal.org issue 3114329: Allow taxonomy menus to contain a
 subset of a vocabulary tree

Signed-off-by: Alex Harries <alex.harries@redactive.co.uk>
---
 config/schema/taxonomy_menu.schema.yml |  3 +++
 src/Entity/TaxonomyMenu.php            | 30 +++++++++++++++++++++++++-
 src/Form/TaxonomyMenuForm.php          | 13 +++++++++++
 3 files changed, 45 insertions(+), 1 deletion(-)

diff --git a/config/schema/taxonomy_menu.schema.yml b/config/schema/taxonomy_menu.schema.yml
index 91f73c0..0ad6043 100644
--- a/config/schema/taxonomy_menu.schema.yml
+++ b/config/schema/taxonomy_menu.schema.yml
@@ -29,3 +29,6 @@ taxonomy_menu.taxonomy_menu.*:
     use_term_weight_order:
       type: boolean
       label: 'Use term weight order'
+    taxonomy_term_parent_tid:
+      type: integer
+      label: 'Taxonomy term parent TID'
diff --git a/src/Entity/TaxonomyMenu.php b/src/Entity/TaxonomyMenu.php
index a5ce174..b83a0a3 100644
--- a/src/Entity/TaxonomyMenu.php
+++ b/src/Entity/TaxonomyMenu.php
@@ -97,6 +97,34 @@ class TaxonomyMenu extends ConfigEntityBase implements TaxonomyMenuInterface {
    */
   protected $description_field_name;
 
+  /**
+   * Optionally, only add a subset of the taxonomy terms to the menu.
+   *
+   * @var int
+   *   The taxonomy term TID to use as a parent, or 0 for the entire tree.
+   */
+  protected $taxonomy_term_parent_tid = 0;
+
+  /**
+   * Get the parent term TID to filter the taxonomy menu tree by.
+   *
+   * @return int
+   *   The taxonomy term TID to use as a parent, or 0 for the entire tree.
+   */
+  public function getTaxonomyTermParentTid(): int {
+    return $this->taxonomy_term_parent_tid;
+  }
+
+  /**
+   * Set the parent term TID to filter the taxonomy menu tree by.
+   *
+   * @param int $taxonomy_term_parent_tid
+   *   The taxonomy term TID to use as a parent, or 0 for the entire tree.
+   */
+  public function setTaxonomyTermParentTid(int $taxonomy_term_parent_tid) {
+    $this->taxonomy_term_parent_tid = $taxonomy_term_parent_tid;
+  }
+
   /**
    * {@inheritdoc}
    */
@@ -188,7 +216,7 @@ class TaxonomyMenu extends ConfigEntityBase implements TaxonomyMenuInterface {
     /* @var $termStorage \Drupal\taxonomy\TermStorageInterface */
     $termStorage = $this->entityTypeManager()->getStorage('taxonomy_term');
     // Load taxonomy terms for tax menu vocab.
-    $terms = $termStorage->loadTree($this->getVocabulary(), 0, $this->getDepth() + 1);
+    $terms = $termStorage->loadTree($this->getVocabulary(), $this->getTaxonomyTermParentTid(), $this->getDepth() + 1);
 
     $links = [];
 
diff --git a/src/Form/TaxonomyMenuForm.php b/src/Form/TaxonomyMenuForm.php
index 7175304..bb3a2b9 100644
--- a/src/Form/TaxonomyMenuForm.php
+++ b/src/Form/TaxonomyMenuForm.php
@@ -169,6 +169,19 @@ class TaxonomyMenuForm extends EntityForm {
       '#default_value' => $taxonomy_menu->getMenuParent(),
     ];
 
+    $form['taxonomy_term_parent_tid'] = [
+      '#type' => 'textfield',
+      '#attributes' => array(
+        // insert space before attribute name :)
+        ' type' => 'number',
+      ),
+      '#title' => $this->t('Only show children of this term TID'),
+      '#description' => $this->t('If you only wish to show a sub-set of a taxonomy vocabulary, you can set the parent term TID here.'),
+      '#default_value' => $taxonomy_menu->getTaxonomyTermParentTid(),
+      '#required' => TRUE,
+      '#maxlength' => 10,
+    ];
+
     // If this checkbox is active, use the term weight for the menu item order.
     // Otherwise the menu items will be sorted alphabetically.
     // The default is order by weight.
-- 
2.21.1 (Apple Git-122.3)

